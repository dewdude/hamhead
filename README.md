HamHead - Remote Head For Your CAT Rig

By: Jay Moore/NQ4T - nq4tango@gmail.com

# History

```
8-Feb-2020: Repository Creation, Intitial Commit, Alpha Versions
31-Aug-2021: Name Change, More Alpha Code, Actually Control Radio
04-Sep-2021: Initialization Routine Alpha

````


# About

HamHead (formerally ArFO) is an Arduino-based program that interfaces with an external display, external controls, and your ham rig;
to allow you the convenience of a remote head for rigs that do not feature one. It is written in Arduino and 
commands your rig using it's CAT/CI-V connection. HamHead is a continuation/reboot of PiFO; a previous project
that sought to do the same thing. 

In addition to being able to control your operating mode, VFO, and a few other select options; HamHead also provides
"advanced logic" for an otherwise older rig. When possible, it uses it's own programming rather than rely on the
rig's internal functions. Planned features include a memory function, similar to what the proof-of-concept PiFO 
was; that will allow you to expand on the memory preset functions of older rigs as well as the ability to carry
your memory presets with you between rigs. HamHead stores the frequency information and sends it to the radio as
a frequency for the VFO. 

HamHead will use it's own internal logic when necessary, meaning older rigs will get some new features. This means
some limitations (like CI-V WARC band data) are irrelevant to HamHead as we can calculate and process things 
ourselves. 

# Why The Change To Arduino

When I originally came up with the idea for PiFO; the Raspberry Pi seemed like the easest development solution. With
existing software in it's repositories the original idea was just a basic python script to interface with these.
However as I began developing it I found a lot of limitations in the existing solutions that did not perform the way
I thought they should, or wanted it to. 

The other problem with the RPi is the amount of time it takes to boot. Even with a basic installation of Rasbian;
20 seconds was how long it took to become active without getting in to the OS and really stripping things down. 
When I created [HiFiLOGIX](https://gitlab.com/dewdude/hifilogix) I decided to go with the Arduino platform due to
almost negligable boot-up time; the goal was to have it booted and ready to go within the 4.5 seconds it took for
the stereo to activate it's speaker output relay. 

The (relative) success of getting HiFoLOGIX to work and armed with new knowledge of Arduino; I decided it should be
possible to build PiFO in a way that works entirely on a microcontroller. We don't need a full OS; and this means
no long boot times. 

# Hardware

Though I have not gotten to the full design of a "reference" remote head; I am shooting for the following:

* easy-to-read seven-segment LED displays with a minimal amount of information
* one (or more) rotary encoders for spinning the VFO/channels
* dedicated buttons for functions that don't require you to navigate a menu
* onboard level converters for your rig's communication protocol
* two cables to the head; power and communications

Currently development is occuring on an Arduino Mega2560 and a simple 7404 based level converter for CI-V. This is
actually the same converter used in the RPi version. If the code allows; the final product may contain an ATMega328.
All hardware is planned to be integrated in to the head, meaning you should only need a power and communication
cable going to your radio.

I will post full pictures and a schematic as I get further along in the development.

Usage of onboard USB controllers in newer rigs will depend on how difficult it is to implement. Unlike the RPi;
the AVR based ATMegas do not feature native USB support and only offer serial communications. So plugging a rig's
USB in to the thing won't work. The chip that handles serial-to-USB on the board would have to be reprogrammed to
act as a USB host and understand all the various serial-to-USB adapters on the market. While USB controllers are
available as an external module; the goal of this project is to keep costs to a minimum. No one wants to spend
$300 on a remote head accessory!

Development for iCom stuff is being done primarily on an IC-7100, with a IC-706MKII and IC-725 to check compatibility.
I won't mention the irony of developing a remote head on a rig with a remote head....or the fact I came up with the
thing when I had no rigs with remote heads and now own two. 

Additional display types may be supported depending on demand or user-submitted code. The project is open-source; you
are welcome to contribute code.

# Software

All sketches are currently in the "alpha" folder. 

This software currently uses two libraries:

LEDMatrixDriver by Bartosz Bielawski - Installable in the ArduinoIDE or from https://github.com/bartoszbielawski/LEDMatrixDriver

Rotary by Ben Buxton - https://github.com/buxtronix/arduino/tree/master/libraries 

A copy of the Rotary library has been included as it is not available in the ArduinoIDE. 

I have also changed from my ususal BSD 2-Clause license to GPL v3 for better compatibility with any libraries. 


# Current DEV Status

Development offically started on 5-FEB-2020 with working out the physical interface betweent he rig and the radio. 
This has been fully tested and I'm able to send and receieve bytes.

Current development is extremely early alpha and is largely working on not just getting iCom CI-V communication
working, but making sure I can actually get everything working the way I want. It's not considered ready for
prime-time and the code will likely undergo reorganization to make customizations to physical UI and radio
drivers easier.

![Current Development Version](/images/civdecode.png "CIV Decoding")*Current Development Version*

Development was dorment from early 2020 until late summer 2021 when it was restarted. The current alpha is now capable
of actually controlling a radio and displaying it's frequency. 

[Video Showing Current Protptype] (https://youtu.be/GGRZIbIPLOM)

CI-V communication is being chipped away on. I now have an initalization routine going that gets the starting vfo,
switches between the vfos, reads the frequency data of each, updates the appropiate variables, and resets the starting
VFO. Echo and ACK responses are handled in getdata(). Support for activating 731 Mode is in code. This outputs some
information to the serial monitor. Debugging routines also left in code but disabled.


# License

    HamHead - Remote Head For Ham Radios
    Copyright (C) 2021 Jay Moore - nq4tango@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


	Rotary encoder handler for arduino. v1.1
	Copyright 2011 Ben Buxton. Licenced under the GNU GPL Version 3. Contact: bb@cactii.net